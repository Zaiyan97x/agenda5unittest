﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Sub()
        {

            //Arrange (Organizar)
            int value1 = 10;
            int value2 = 3;
            int expected = 7;

            //Act (Actuar)


            int actual = SystemUnderTest.Sub(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }


        [TestMethod]
        public void Mul()
        {

            //Arrange (Organizar)
            int value1 = 3;
            int value2 = 7;
            int expected = 21;

            //Act (Actuar)


            int actual = SystemUnderTest.Mul(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }


        [TestMethod]
        public void Div()
        {

            //Arrange (Organizar)
            double value1 = 10;
            double value2 = 4;
            double expected = 2.5;

            //Act (Actuar)


            double actual = SystemUnderTest.Div(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<double>(expected, actual, "Error, valores no coinciden");

        }
    }
}
